(function ($) {
    $(document).ready(function () {
        $('.btn-read-more > button').on('click', function () {
            $('.read-more').slideToggle(500);
        });
    });
}(jQuery));