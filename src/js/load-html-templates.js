(function ($) {
    $(document).ready(function () {
        if ($('#article').attr('data-template-url') !== '') {
            var url = $('#article').attr('data-template-url');
            $('#article').load(url, function () {
                findKeywords();
            });
        }
    });
}(jQuery));